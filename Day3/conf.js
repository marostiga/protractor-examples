var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
    seleniumAdress: 'http://localhost:4444/wd/hub',
    specs: ['NonAngApp.js'],

    onPrepare: function() {
        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
        jasmine.getEnv().addReporter(new HtmlReporter({
           baseDirectory: 'tmp/screenshots',
           screenshotsSubfolder: 'images',
           takeScreenShotsOnlyForFailedSpecs: false,
           disableScreenshots: false,
           docTitle: 'my reporter',
           docName: 'index.html'
           
        }).getJasmine2Reporter());
     }

};