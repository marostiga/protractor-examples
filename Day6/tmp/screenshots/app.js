var app = angular.module('reportingApp', []);

//<editor-fold desc="global helpers">

var isValueAnArray = function (val) {
    return Array.isArray(val);
};

var getSpec = function (str) {
    var describes = str.split('|');
    return describes[describes.length - 1];
};
var checkIfShouldDisplaySpecName = function (prevItem, item) {
    if (!prevItem) {
        item.displaySpecName = true;
    } else if (getSpec(item.description) !== getSpec(prevItem.description)) {
        item.displaySpecName = true;
    }
};

var getParent = function (str) {
    var arr = str.split('|');
    str = "";
    for (var i = arr.length - 2; i > 0; i--) {
        str += arr[i] + " > ";
    }
    return str.slice(0, -3);
};

var getShortDescription = function (str) {
    return str.split('|')[0];
};

var countLogMessages = function (item) {
    if ((!item.logWarnings || !item.logErrors) && item.browserLogs && item.browserLogs.length > 0) {
        item.logWarnings = 0;
        item.logErrors = 0;
        for (var logNumber = 0; logNumber < item.browserLogs.length; logNumber++) {
            var logEntry = item.browserLogs[logNumber];
            if (logEntry.level === 'SEVERE') {
                item.logErrors++;
            }
            if (logEntry.level === 'WARNING') {
                item.logWarnings++;
            }
        }
    }
};

var defaultSortFunction = function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) {
        return -1;
    }
    else if (a.sessionId > b.sessionId) {
        return 1;
    }

    if (a.timestamp < b.timestamp) {
        return -1;
    }
    else if (a.timestamp > b.timestamp) {
        return 1;
    }

    return 0;
};


//</editor-fold>

app.controller('ScreenshotReportController', function ($scope, $http) {
    var that = this;
    var clientDefaults = {};

    $scope.searchSettings = Object.assign({
        description: '',
        allselected: true,
        passed: true,
        failed: true,
        pending: true,
        withLog: true
    }, clientDefaults.searchSettings || {}); // enable customisation of search settings on first page hit

    this.warningTime = 1400;
    this.dangerTime = 1900;

    var initialColumnSettings = clientDefaults.columnSettings; // enable customisation of visible columns on first page hit
    if (initialColumnSettings) {
        if (initialColumnSettings.displayTime !== undefined) {
            // initial settings have be inverted because the html bindings are inverted (e.g. !ctrl.displayTime)
            this.displayTime = !initialColumnSettings.displayTime;
        }
        if (initialColumnSettings.displayBrowser !== undefined) {
            this.displayBrowser = !initialColumnSettings.displayBrowser; // same as above
        }
        if (initialColumnSettings.displaySessionId !== undefined) {
            this.displaySessionId = !initialColumnSettings.displaySessionId; // same as above
        }
        if (initialColumnSettings.displayOS !== undefined) {
            this.displayOS = !initialColumnSettings.displayOS; // same as above
        }
        if (initialColumnSettings.inlineScreenshots !== undefined) {
            this.inlineScreenshots = initialColumnSettings.inlineScreenshots; // this setting does not have to be inverted
        } else {
            this.inlineScreenshots = false;
        }
        if (initialColumnSettings.warningTime) {
            this.warningTime = initialColumnSettings.warningTime;
        }
        if (initialColumnSettings.dangerTime){
            this.dangerTime = initialColumnSettings.dangerTime;
        }
    }

    this.showSmartStackTraceHighlight = true;

    this.chooseAllTypes = function () {
        var value = true;
        $scope.searchSettings.allselected = !$scope.searchSettings.allselected;
        if (!$scope.searchSettings.allselected) {
            value = false;
        }

        $scope.searchSettings.passed = value;
        $scope.searchSettings.failed = value;
        $scope.searchSettings.pending = value;
        $scope.searchSettings.withLog = value;
    };

    this.isValueAnArray = function (val) {
        return isValueAnArray(val);
    };

    this.getParent = function (str) {
        return getParent(str);
    };

    this.getSpec = function (str) {
        return getSpec(str);
    };

    this.getShortDescription = function (str) {
        return getShortDescription(str);
    };

    this.convertTimestamp = function (timestamp) {
        var d = new Date(timestamp),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh === 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    };


    this.round = function (number, roundVal) {
        return (parseFloat(number) / 1000).toFixed(roundVal);
    };


    this.passCount = function () {
        var passCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.passed) {
                passCount++;
            }
        }
        return passCount;
    };


    this.pendingCount = function () {
        var pendingCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.pending) {
                pendingCount++;
            }
        }
        return pendingCount;
    };


    this.failCount = function () {
        var failCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (!result.passed && !result.pending) {
                failCount++;
            }
        }
        return failCount;
    };

    this.passPerc = function () {
        return (this.passCount() / this.totalCount()) * 100;
    };
    this.pendingPerc = function () {
        return (this.pendingCount() / this.totalCount()) * 100;
    };
    this.failPerc = function () {
        return (this.failCount() / this.totalCount()) * 100;
    };
    this.totalCount = function () {
        return this.passCount() + this.failCount() + this.pendingCount();
    };

    this.applySmartHighlight = function (line) {
        if (this.showSmartStackTraceHighlight) {
            if (line.indexOf('node_modules') > -1) {
                return 'greyout';
            }
            if (line.indexOf('  at ') === -1) {
                return '';
            }

            return 'highlight';
        }
        return true;
    };

    var results = [
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "299f199d9b0d8aa6d3ade42fff7b71fb",
        "instanceId": 12443,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566399329728,
                "type": ""
            }
        ],
        "timestamp": 1566399329493,
        "duration": 2421
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "6700a7829b06bf5e1b94b3d7c934e8e1",
        "instanceId": 12750,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Expected '7' to equal '8'."
        ],
        "trace": [
            "Error: Failed expectation\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/protractor-day2/protractorTeste.js:15:63)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566399348096,
                "type": ""
            }
        ],
        "screenShotFile": "images/005300c8-0097-0018-00a8-00dc004d00c5.png",
        "timestamp": 1566399347858,
        "duration": 2369
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "6cd2deb2f8d11955725b6b6103b7bd7d",
        "instanceId": 19105,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Expected '7' to equal '8'."
        ],
        "trace": [
            "Error: Failed expectation\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day2/protractorTest.js:15:63)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566408798520,
                "type": ""
            }
        ],
        "screenShotFile": "images/00bb00e3-00e2-00bd-004c-00c000670066.png",
        "timestamp": 1566408797467,
        "duration": 3078
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "dbf00eff983b53a534e4cb10b8cdcac1",
        "instanceId": 8160,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566485147869,
                "type": ""
            }
        ],
        "timestamp": 1566485147294,
        "duration": 2602
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "ba5901391e68e9e093e55516463bfd64",
        "instanceId": 8459,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566485163409,
                "type": ""
            }
        ],
        "timestamp": 1566485163164,
        "duration": 2383
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "b5a648768dcdef5756824c4bfd6b921d",
        "instanceId": 3480,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Expected '.' to equal '7'."
        ],
        "trace": [
            "Error: Failed expectation\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:24:63)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495340744,
                "type": ""
            }
        ],
        "screenShotFile": "images/00510026-00ac-004e-00db-00c900290075.png",
        "timestamp": 1566495340091,
        "duration": 720
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "18f54312e8b680a9cf64b432059a1b93",
        "instanceId": 3781,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495354618,
                "type": ""
            }
        ],
        "timestamp": 1566495354355,
        "duration": 2484
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "ae4af12473f49527fddee75c7df8022c",
        "instanceId": 4077,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495375179,
                "type": ""
            }
        ],
        "timestamp": 1566495374719,
        "duration": 2718
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "6268a14aa56fcb1b8c202787b5a357e0",
        "instanceId": 4401,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495430863,
                "type": ""
            }
        ],
        "timestamp": 1566495430554,
        "duration": 2690
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "7d6bb180995018a156ae470fc61b7835",
        "instanceId": 4712,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495458560,
                "type": ""
            }
        ],
        "timestamp": 1566495458289,
        "duration": 2671
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "00692e71ffc14a2963963d9c599d7cfa",
        "instanceId": 5027,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495508561,
                "type": ""
            }
        ],
        "timestamp": 1566495508196,
        "duration": 2617
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "cf7ca62317797029425295c338a328b4",
        "instanceId": 6489,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Failed: Cannot read property 'firstFld' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'firstFld' of undefined\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:19:18)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7\n    at <anonymous>\nFrom: Task: Run it(\"first function\") in control flow\n    at UserContext.<anonymous> (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:94:19)\n    at attempt (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4297:26)\n    at QueueRunner.run (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4217:20)\n    at runNext (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4257:20)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4264:13\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4172:9\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:64:48\n    at ControlFlow.emit (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/events.js:62:21)\n    at ControlFlow.shutdown_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2674:10)\n    at shutdownTask_.MicroTask (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:17:5)\n    at addSpecsToSuite (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1107:25)\n    at Env.describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1074:7)\n    at describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4399:18)\n    at Object.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:3:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566495951905,
                "type": ""
            }
        ],
        "screenShotFile": "images/006e007f-0093-00c9-00e5-001600150041.png",
        "timestamp": 1566495951561,
        "duration": 429
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "ebd844aac00e2336ea454c78fec58c41",
        "instanceId": 7075,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Failed: Cannot read property 'sendKeys' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'sendKeys' of undefined\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:22:27)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7\n    at <anonymous>\nFrom: Task: Run it(\"first function\") in control flow\n    at UserContext.<anonymous> (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:94:19)\n    at attempt (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4297:26)\n    at QueueRunner.run (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4217:20)\n    at runNext (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4257:20)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4264:13\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4172:9\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:64:48\n    at ControlFlow.emit (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/events.js:62:21)\n    at ControlFlow.shutdown_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2674:10)\n    at shutdownTask_.MicroTask (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:20:5)\n    at addSpecsToSuite (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1107:25)\n    at Env.describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1074:7)\n    at describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4399:18)\n    at Object.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:3:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496043799,
                "type": ""
            }
        ],
        "screenShotFile": "images/00c80098-0081-0021-00c5-00540098005f.png",
        "timestamp": 1566496043302,
        "duration": 723
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "da844f054c50c331837b7f94cf694913",
        "instanceId": 7668,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Failed: Cannot read property 'sendKeys' of undefined"
        ],
        "trace": [
            "TypeError: Cannot read property 'sendKeys' of undefined\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:22:27)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7\n    at <anonymous>\nFrom: Task: Run it(\"first function\") in control flow\n    at UserContext.<anonymous> (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:94:19)\n    at attempt (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4297:26)\n    at QueueRunner.run (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4217:20)\n    at runNext (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4257:20)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4264:13\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4172:9\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:64:48\n    at ControlFlow.emit (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/events.js:62:21)\n    at ControlFlow.shutdown_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2674:10)\n    at shutdownTask_.MicroTask (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:20:5)\n    at addSpecsToSuite (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1107:25)\n    at Env.describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1074:7)\n    at describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4399:18)\n    at Object.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:3:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496148511,
                "type": ""
            }
        ],
        "screenShotFile": "images/00c400da-0085-00d6-00d9-00ab00890027.png",
        "timestamp": 1566496148255,
        "duration": 490
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "e505f6e1e8ad662481a84a4342ad8ee9",
        "instanceId": 7992,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496364806,
                "type": ""
            }
        ],
        "timestamp": 1566496364505,
        "duration": 2569
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "4bd8f89d85232fb52ba84a6941ed070f",
        "instanceId": 8615,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Failed: testPage.resultFld.getText(...).toEqual is not a function"
        ],
        "trace": [
            "TypeError: testPage.resultFld.getText(...).toEqual is not a function\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:26:38)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7\n    at <anonymous>\nFrom: Task: Run it(\"first function\") in control flow\n    at UserContext.<anonymous> (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:94:19)\n    at attempt (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4297:26)\n    at QueueRunner.run (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4217:20)\n    at runNext (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4257:20)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4264:13\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4172:9\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:64:48\n    at ControlFlow.emit (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/events.js:62:21)\n    at ControlFlow.shutdown_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2674:10)\n    at shutdownTask_.MicroTask (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:20:5)\n    at addSpecsToSuite (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1107:25)\n    at Env.describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1074:7)\n    at describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4399:18)\n    at Object.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:3:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496784874,
                "type": ""
            }
        ],
        "screenShotFile": "images/002900d0-0008-0010-0059-003a00e70090.png",
        "timestamp": 1566496784354,
        "duration": 738
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "bfff7005de496be345232ff9831c1647",
        "instanceId": 8937,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Failed: No element found using locator: by.model(\"gobutton\")"
        ],
        "trace": [
            "NoSuchElementError: No element found using locator: by.model(\"gobutton\")\n    at elementArrayFinder.getWebElements.then (/usr/local/lib/node_modules/protractor/built/element.js:814:27)\n    at ManagedPromise.invokeCallback_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1376:14)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2927:27)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)Error\n    at ElementArrayFinder.applyAction_ (/usr/local/lib/node_modules/protractor/built/element.js:459:27)\n    at ElementArrayFinder.(anonymous function).args [as click] (/usr/local/lib/node_modules/protractor/built/element.js:91:29)\n    at ElementFinder.(anonymous function).args [as click] (/usr/local/lib/node_modules/protractor/built/element.js:831:22)\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:24:24)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\nFrom: Task: Run it(\"first function\") in control flow\n    at UserContext.<anonymous> (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:94:19)\n    at attempt (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4297:26)\n    at QueueRunner.run (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4217:20)\n    at runNext (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4257:20)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4264:13\n    at /usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4172:9\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:64:48\n    at ControlFlow.emit (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/events.js:62:21)\n    at ControlFlow.shutdown_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2674:10)\n    at shutdownTask_.MicroTask (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:20:5)\n    at addSpecsToSuite (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1107:25)\n    at Env.describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:1074:7)\n    at describe (/usr/local/lib/node_modules/protractor/node_modules/jasmine-core/lib/jasmine-core/jasmine.js:4399:18)\n    at Object.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:3:1)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496831461,
                "type": ""
            }
        ],
        "screenShotFile": "images/008200bb-00cf-005e-0062-00dd004300b9.png",
        "timestamp": 1566496831072,
        "duration": 759
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "01cd7a14d91370bcac6b19b98ef646ca",
        "instanceId": 9267,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566496865107,
                "type": ""
            }
        ],
        "timestamp": 1566496864769,
        "duration": 2585
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "6ece7e7ca0b1416bc059dc15e8ea067c",
        "instanceId": 9809,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566497523261,
                "type": ""
            }
        ],
        "timestamp": 1566497522796,
        "duration": 2896
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "d4466bf4af91f22488112c65face7370",
        "instanceId": 10124,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566497536242,
                "type": ""
            }
        ],
        "timestamp": 1566497535934,
        "duration": 2569
    },
    {
        "description": "first function|My first protractor test",
        "passed": false,
        "pending": false,
        "sessionId": "3d0305ef14991b1c375523d7a2d9eb17",
        "instanceId": 10435,
        "browser": {
            "name": "chrome"
        },
        "message": [
            "Expected '.' to equal '7'."
        ],
        "trace": [
            "Error: Failed expectation\n    at UserContext.<anonymous> (/home/beijo/Área de Trabalho/Protractor/Day6/pageObjects.js:25:46)\n    at /usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:112:25\n    at new ManagedPromise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:1077:7)\n    at ControlFlow.promise (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2505:12)\n    at schedulerExecute (/usr/local/lib/node_modules/protractor/node_modules/jasminewd2/index.js:95:18)\n    at TaskQueue.execute_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3084:14)\n    at TaskQueue.executeNext_ (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:3067:27)\n    at asyncRun (/usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:2974:25)\n    at /usr/local/lib/node_modules/protractor/node_modules/selenium-webdriver/lib/promise.js:668:7"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566497560712,
                "type": ""
            }
        ],
        "screenShotFile": "images/00860063-00b5-00d1-00c1-00db00aa006b.png",
        "timestamp": 1566497560334,
        "duration": 536
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "75366b76c5d2226c9e84e7cd432d9f81",
        "instanceId": 10728,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566497592169,
                "type": ""
            }
        ],
        "timestamp": 1566497591847,
        "duration": 2716
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "7662f3c374fde6927d6668aead3f078f",
        "instanceId": 11067,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566497826407,
                "type": ""
            }
        ],
        "timestamp": 1566497826103,
        "duration": 2685
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "aa92a54ba6d8daa1639422c454f09069",
        "instanceId": 6851,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566570757048,
                "type": ""
            }
        ],
        "timestamp": 1566570756326,
        "duration": 3387
    },
    {
        "description": "first function|My first protractor test",
        "passed": true,
        "pending": false,
        "sessionId": "f6ecb088106da8acfb2464e8a5767df9",
        "instanceId": 7164,
        "browser": {
            "name": "chrome"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "https://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 ()",
                "timestamp": 1566570828005,
                "type": ""
            }
        ],
        "timestamp": 1566570827519,
        "duration": 3153
    }
];

    this.sortSpecs = function () {
        this.results = results.sort(function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) return -1;else if (a.sessionId > b.sessionId) return 1;

    if (a.timestamp < b.timestamp) return -1;else if (a.timestamp > b.timestamp) return 1;

    return 0;
});
    };

    this.loadResultsViaAjax = function () {

        $http({
            url: './combined.json',
            method: 'GET'
        }).then(function (response) {
                var data = null;
                if (response && response.data) {
                    if (typeof response.data === 'object') {
                        data = response.data;
                    } else if (response.data[0] === '"') { //detect super escaped file (from circular json)
                        data = CircularJSON.parse(response.data); //the file is escaped in a weird way (with circular json)
                    }
                    else {
                        data = JSON.parse(response.data);
                    }
                }
                if (data) {
                    results = data;
                    that.sortSpecs();
                }
            },
            function (error) {
                console.error(error);
            });
    };


    if (clientDefaults.useAjax) {
        this.loadResultsViaAjax();
    } else {
        this.sortSpecs();
    }


});

app.filter('bySearchSettings', function () {
    return function (items, searchSettings) {
        var filtered = [];
        if (!items) {
            return filtered; // to avoid crashing in where results might be empty
        }
        var prevItem = null;

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.displaySpecName = false;

            var isHit = false; //is set to true if any of the search criteria matched
            countLogMessages(item); // modifies item contents

            var hasLog = searchSettings.withLog && item.browserLogs && item.browserLogs.length > 0;
            if (searchSettings.description === '' ||
                (item.description && item.description.toLowerCase().indexOf(searchSettings.description.toLowerCase()) > -1)) {

                if (searchSettings.passed && item.passed || hasLog) {
                    isHit = true;
                } else if (searchSettings.failed && !item.passed && !item.pending || hasLog) {
                    isHit = true;
                } else if (searchSettings.pending && item.pending || hasLog) {
                    isHit = true;
                }
            }
            if (isHit) {
                checkIfShouldDisplaySpecName(prevItem, item);

                filtered.push(item);
                prevItem = item;
            }
        }

        return filtered;
    };
});

