var TestPage = require('./page-objects/test_page')

describe('My first protractor test', function() {
    var testPage

    beforeEach(function () {
        browser.ignoreSynchronization = false
        console.log("Before Each")

        testPage = new TestPage()

        browser.get('https://juliemr.github.io/protractor-demo/');
    })

    afterEach(function () {
        console.log("After Each")
    })

    it('first function', function() {
        console.log("Test Case")
        testPage.firstFld.sendKeys('5');
        testPage.secoundFld.sendKeys('2');
        testPage.goBtn.click();

        expect(testPage.resultFld.getText()).toEqual('7');


    });

});
