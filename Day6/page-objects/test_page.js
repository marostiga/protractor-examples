var TestPage = function () {}

TestPage.prototype = Object.create({}, {
    firstFld: {get: function () { return element(by.model('first')) }},
    secoundFld: {get: function () { return element(by.model('second')) }},
    goBtn: {get: function () { return element(by.id('gobuttonVamos lá galera')) }},
    resultFld: {get: function () { return element(by.className('ng-binding')) }}

})

module.exports = TestPage