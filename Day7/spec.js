describe('Login no mktzap', () => {


    beforeEach(() => {
        browser.get('https://qa-poli.infranw.com.br/login');
    });

    it('Responder e finalizar atendimento', () => {
       
        element(by.id('login_fldLoginUserName')).sendKeys('gestor1@teste.com');
        element(by.id('login_fldLoginPassword')).sendKeys('123');
        element(by.id('login_btnLogin')).click();

        element.all(by.className('accordion-title ng-binding')).get(4).click();
        
        element(by.partialLinkText('Analítico')).click();

        // get today's date
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        console.log(today);


        var inputEle = element(by.model("vm.filters.fromDate"));
        var inputEle2 = element(by.model("vm.filters.toDate"));

        browser.executeScript('arguments[0].value=arguments[1]', inputEle.getWebElement(), today);
        browser.executeScript('arguments[0].value=arguments[1]', inputEle2.getWebElement(), today);

        element(by.className('button expand')).click();

        //browser.sleep(5000);

    });

    afterEach(() => {

      });
}); 