describe('login novo usuário', () => {
  
  let EC = protractor.ExpectedConditions;
  
  let url = 'https://stage-cetelem.qanw.com.br/login';
  
  let user = element.all(by.id('login_fldUsername')).get(0); 
  let passWord = element(by.id('login_fldPassword'));
  let btnLogin = element.all(by.id('login_btnLogin')).get(0);
  let btnLogin_1 = element.all(by.id('login_btnLogin')).get(1);
  let logout = element(by.id('header_btnLogout'));
  let btnInsertUser = element.all(by.id('btn_cadUser')).get(0);
  let fldNameUser = element(by.id('name'));
  
  let menuConfiguracoes = element.all(by.className('accordion-title ng-binding')).get(1);
  let menuMinhaEmpresa = element.all(by.partialLinkText('Minha empresa')).get(0);
  let abaUsuarios = element.all(by.className('tab-item ng-binding ng-scope ng-isolate-scope')).get(2);
  let fldEmailUser = element(by.id('email'));
  let fldnomeExibicao = element(by.id('display_name'));
  let fldPasswordUser = element(by.id('password'));
  let generoMasculino = '1';
  let generoFeminino = element(by.id('0'));
  let funcaoUser = element(by.id('role'));
  let funcao = 'ng-binding ng-scope';
  let btnSaveUser = element(by.id('btnUsuarioSalvar'));
  let newPassword = element(by.id('login_fldNewPassword'));
  let comfirmPassword = element(by.id('login_fldConfirmPassword'));
  let emailUser = Math.random().toString(36).substring(2, 15)+"@teste.com";
  //let emailUser = 'emailnovousuario99@teste.com';

  beforeEach(() => {
      
        browser.get(url);
        browser.waitForAngularEnabled(true);
        browser.driver.manage().window().maximize();

      });
    
    it ('logar como gestor e criar um novo usuário, depois deslogar', () => {
      
      //Login
      user.sendKeys('gestorbdd@teste.com'); 
      passWord.sendKeys('1'); 
      btnLogin.click(); 
      browser.waitForAngularEnabled(false);
      
      //Valida url atual
      browser.wait(EC.urlIs('https://stage-cetelem.qanw.com.br/atendimento'), 15000);
      expect(browser.getCurrentUrl()).toEqual('https://stage-cetelem.qanw.com.br/atendimento');
    
      //Menu > configurações > Minha Empresa > usuarios
      browser.wait(EC.invisibilityOf(element(by.className('loading'))), 5000)
      menuConfiguracoes.click();
      //waitToClick(menuConfiguracoes, 7000, "Menu Configurações")
      waitToClick(menuMinhaEmpresa, 7000, 'Menu Minha Empresa');
      //browser.sleep(3000);
      //abaUsuarios.click();
      browser.wait(EC.invisibilityOf(element(by.className('loading'))), 5000)
      waitToClick(abaUsuarios, 7000, "Aba usuário")

      //Cadastrar novo usuario
      waitToClick(btnInsertUser, 7000, 'Custom Error Carregamento botão inserir');
      waitToSendKey(fldNameUser, 'Nome do novo usuário', 7000, 'Custom Error Carregamento do campo nome do usuario');
      waitToSendKey(fldEmailUser, emailUser, 7000, 'Erro ao carregar o campo do email do usuário');
      waitToSendKey(fldnomeExibicao, 'Nome do novo usuário', 7000, 'Erro ao carregar o campo nome de exibição');
      waitToSendKey(fldPasswordUser, '123Mudar', 7000, 'Erro ao carregar o campo senha');
      generoFeminino.click();
      funcaoUser.click();
      funcaoUser.sendKeys('Operador');
      funcaoUser.click();
      btnSaveUser.click();

      //Deslogar
      browser.sleep(3000);
      logout.click();
      browser.wait(EC.urlIs(url), 15000);
      expect(browser.getCurrentUrl()).toEqual(url);

      //Login do novo Usuário
      waitToSendKey(user, emailUser, 7000, 'Erro ao carregar o campo email');
      passWord.sendKeys('123Mudar');
      btnLogin.click();
      waitToSendKey(newPassword, '1', 7000, 'Erro ao carregar o campo nova senha');
      comfirmPassword.sendKeys('1');
      btnLogin_1.click();
      
      //Valida Login
      browser.wait(EC.urlIs('https://stage-cetelem.qanw.com.br/atendimento'), 15000);
      expect(browser.getCurrentUrl()).toEqual('https://stage-cetelem.qanw.com.br/atendimento');
           
});

//Função que aguarda o click utilizando Wait do Protractor
function waitToClick(what, timeout, customMessage){
  browser.wait(EC.visibilityOf(what), timeout ,"visibilityOf - Elemento não encontrado! "+customMessage);
  browser.wait(EC.presenceOf(what), timeout ,"presenceOf - Elemento não encontrado! "+customMessage);

  what.click();
}

//Função que aguarda o sendKeys utilizando Wait do Protractor
function waitToSendKey(what, value, timeout, customMessage){
    browser.wait(EC.visibilityOf(what), timeout ,"Elemento não encontrado! "+customMessage);
    what.sendKeys(value);
}


//desconsiderar
/*
function TesteClick (elemento, contador) {

  for( let i = 0; i < contador; ++i ){
    
  }
}*/

});