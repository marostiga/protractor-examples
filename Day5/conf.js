var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
    seleniumAdress: 'http://localhost:4444/wd/hub',
    
    specs: ['headlessMode.js'],
      
    capabilities: {
       browserName: 'chrome',

       chromeOptions:{ 
          //args: [ "--headless", "--disable-gpu", "--window-size=800,600" ]
          args: [ "--headless" ]
      }
    },
    /*multiCapabilities: [
         { 'browserName': 'firefox'}, 
         { 'browserName': 'chrome' } 
    
      ],
   */

    onPrepare: function() {
        // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
        jasmine.getEnv().addReporter(new HtmlReporter({
           baseDirectory: 'tmp/screenshots',
           screenshotsSubfolder: 'images',
           takeScreenShotsOnlyForFailedSpecs: false,
           disableScreenshots: false,
           docTitle: 'my reporter',
           docName: 'index.html'
           
        }).getJasmine2Reporter());
     }

};