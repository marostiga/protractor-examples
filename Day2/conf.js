var HtmlReporter = require('protractor-beautiful-reporter');

exports.config = {
   
   framework: 'jasmine2',
   seleniumAdress: 'http://localhost:4444/wd/hub',
   specs: ['protractorTest.js'],

   onPrepare: function() {
       // Add a screenshot reporter and store screenshots to `/tmp/screenshots`:
      jasmine.getEnv().addReporter(new HtmlReporter({
           baseDirectory: 'tmp/screenshots',
           screenshotsSubfolder: 'images',
           takeScreenShotsOnlyForFailedSpecs: true,
           disableScreenshots: false,
           docTitle: 'my reporter',
           docName: 'index.html'
           
        }).getJasmine2Reporter());
     },
     
   jasmineNodeOpts: {
      showColors: true
   }

};