describe('My first protractor test', function() {
    
    it('first function', function() {
        
        browser.driver.get('https://juliemr.github.io/protractor-demo/');
        
        var firstField = element(by.model('first'));
        var secondField = element(by.model('second'));
        var buttonGo = element(by.id('gobutton'));

        firstField.sendKeys('5');
        secondField.sendKeys('2');
        buttonGo.click();

        expect(element(by.className('ng-binding')).getText()).toEqual('7');


    });

    it('first function', function() {
        
        browser.driver.get('https://juliemr.github.io/protractor-demo/');
        
        var firstField = element(by.model('first'));
        var secondField = element(by.model('second'));
        var buttonGo = element(by.id('gobutton'));

        firstField.sendKeys('8');
        secondField.sendKeys('2');
        buttonGo.click();

        expect(element(by.className('ng-binding')).getText()).toEqual('10');


    });

});
